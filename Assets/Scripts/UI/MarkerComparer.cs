﻿using System.Collections.Generic;

namespace Slar.UI
{
	public class MarkerComparer : Comparer<MapMarker>
	{
		public override int Compare(MapMarker x, MapMarker y)
		{
			var xId = x.GetInstanceID();
			var yId = y.GetInstanceID();

			if (xId == yId) return 0;

			var xPos = x.RectTransform.anchoredPosition.y;
			var yPos = y.RectTransform.anchoredPosition.y;

			if (xPos > yPos) return -1;
			if (yPos > xPos) return 1;

			return -(xId.CompareTo(yId));
		}
	}
}