using System;
using UnityEngine;
using UnityEngine.UI;

namespace Slar.UI
{
	public class MapMarker : MonoBehaviour
	{
		[SerializeField] private Image _image;


		public RectTransform RectTransform => transform as RectTransform;
		public float Radius => RectTransform.rect.width / 2;


		public void Set(Sprite sprite, Color color)
		{
			_image.sprite = sprite;
			_image.color  = color;
		}

		public void SetPosition(Vector2 pos) => RectTransform.anchoredPosition = pos;
	}
}