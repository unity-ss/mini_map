using System.Collections.Generic;
using UnityEngine;

namespace Slar.UI
{
	public class MapMarkersPool : MonoBehaviour
	{
		private readonly Stack<MapMarker> _markersPool = new Stack<MapMarker>();


		[SerializeField] private MapMarker _mapMarkerPrefab;


		public MapMarker MapMarkerPrefab => _mapMarkerPrefab;


		private bool HasFreeMarker => _markersPool.Count > 0;


		public MapMarker GetMarker()
		{
			var marker = HasFreeMarker ? GetMarkerFormPool() : CreateNewMarker();
			return marker;
		}


		private MapMarker GetMarkerFormPool()
		{
			var marker = _markersPool.Pop();

			return marker;
		}

		private MapMarker CreateNewMarker()
		{
			var marker = Instantiate(_mapMarkerPrefab, transform);

			return marker;
		}

		public void PutMarkerBack(MapMarker marker)
		{
			marker.RectTransform.SetParent(transform);

			_markersPool.Push(marker);
		}
	}
}