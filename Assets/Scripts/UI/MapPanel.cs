﻿using System.Collections.Generic;
using Slar.Virtual;
using UnityEngine;

namespace Slar.UI
{
	public class MapPanel : MonoBehaviour
	{
		private readonly SortedList<MapMarker, MapMarker> _markers =
			new SortedList<MapMarker, MapMarker>(new MarkerComparer());


		[SerializeField] private RectTransform _root;
		[SerializeField] private MapMarkersPool _pool;
		[SerializeField] private RectTransform _playerDot;


		public float PixelsPerWorldUnit { get; set; }
		public float RadarRadius => _root.rect.width          / 2;
		public float PlayerDotRadius => _playerDot.rect.width / 2;


		public void AddMarker(VirtualMapMarker virtualMarker)
		{
			var marker = _pool.GetMarker();

			marker.RectTransform.SetParent(_root);

			var pos = MarkerWorldToMapPosition(virtualMarker.RelativePosition);
			marker.SetPosition(pos);

			marker.Set(virtualMarker.Sprite, virtualMarker.Color);
			marker.name = "MapMarker_" + virtualMarker.name;

			marker.gameObject.SetActive(true);

			_markers.Add(marker, marker);

			virtualMarker.ListenTriggerExit(virtualMapMarker =>
			{
				virtualMapMarker.UnlistenTriggerExit();
				RemoveMarker(marker);
			});

			var markersKeys = _markers.Keys;
			for (var i = 0; i < markersKeys.Count; ++i)
			{
				markersKeys[i].transform.SetSiblingIndex(i);
			}
		}

		public void UpdateMarkersRootPosition(Vector2 relativePos)
		{
			_root.anchoredPosition = WorldToMapPosition(relativePos);
		}


		private Vector2 MarkerWorldToMapPosition(Vector2 relativePosition)
		{
			var worldToMapPosition = WorldToMapPosition(relativePosition);
			return worldToMapPosition - _root.anchoredPosition;
		}

		private Vector2 WorldToMapPosition(Vector2 relativePosition) => relativePosition * PixelsPerWorldUnit;

		private void RemoveMarker(MapMarker marker)
		{
			var count = _markers.Count;
			_markers.Remove(marker);
			if (count == _markers.Count)
			{
				Debug.LogError($"[{nameof(MapPanel)}] marker [{marker.name}] wasn't removed");
			}

			_pool.PutMarkerBack(marker);
			marker.gameObject.SetActive(false);
		}
	}
}