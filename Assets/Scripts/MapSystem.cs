﻿using Slar.UI;
using Slar.Virtual;
using UnityEngine;

namespace Slar
{
	public class MapSystem : MonoBehaviour
	{
		[SerializeField] private VirtualMap _virtualMap;
		[SerializeField] private MapPanel _mapPanel;
		[SerializeField] private MapMarkersPool _mapMarkersPool;


		private Vector2 _prevPlayerPos;


		private void Awake()
		{
			_mapPanel.PixelsPerWorldUnit =  _mapPanel.RadarRadius / _virtualMap.Player.ColliderRadius;
			_virtualMap.OnMarkerInRadius += AddMarker;

			var playerMarkerSize = _mapPanel.PlayerDotRadius       / _mapPanel.PixelsPerWorldUnit;
			var scale            = playerMarkerSize / _virtualMap.Player.SpriteRadius;
			_virtualMap.Player.SetScale(scale);

			var markerSize = _mapMarkersPool.MapMarkerPrefab.Radius / _mapPanel.PixelsPerWorldUnit;
			_virtualMap.Init(markerSize);
		}

		private void Update()
		{
			var playerWorldPosition = _prevPlayerPos      - _virtualMap.Player.WorldPosition;
			var relativePos         = playerWorldPosition - _prevPlayerPos;

			_prevPlayerPos = _virtualMap.Player.WorldPosition;

			if (relativePos != Vector2.zero)
			{
				_mapPanel.UpdateMarkersRootPosition(relativePos);
			}
		}

		private void AddMarker(VirtualMapMarker virtualMarker)
		{
			_mapPanel.AddMarker(virtualMarker);
		}
	}
}