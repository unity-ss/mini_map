﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Slar.Virtual
{
	public class VirtualMap : MonoBehaviour
	{
		[SerializeField] private VirtualMapPlayer _player;
		[SerializeField] private List<VirtualMapMarker> _markers;


		public event Action<VirtualMapMarker> OnMarkerInRadius;


		public VirtualMapPlayer Player => _player;


		public void Init(float markerSize)
		{
			foreach (var marker in _markers)
			{
				marker.Init();
				marker.ListenTriggerEnter(OnMarkerTriggerEnter);

				var scale = markerSize / marker.ColliderRadius;
				marker.SetScale(scale);
			}
		}


		private void OnMarkerTriggerEnter(VirtualMapMarker marker)
		{
			marker.RelativePosition = marker.WorldPosition - _player.WorldPosition;
			OnMarkerInRadius?.Invoke(marker);
		}
	}
}