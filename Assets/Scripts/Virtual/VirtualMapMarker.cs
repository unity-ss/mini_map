﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Slar.Virtual
{
	public class VirtualMapMarker : AVirtualMapMarker
	{
		[CanBeNull] private Action<VirtualMapMarker> _onTriggerEnter;
		[CanBeNull] private Action<VirtualMapMarker> _onTriggerExit;


		public void ListenTriggerEnter(Action<VirtualMapMarker> action) => _onTriggerEnter = action;

		public void ListenTriggerExit(Action<VirtualMapMarker> action) => _onTriggerExit = action;

		public void UnlistenTriggerEnter() => _onTriggerEnter = null;

		public void UnlistenTriggerExit() => _onTriggerExit = null;


		private void OnTriggerEnter2D(Collider2D col)
		{
			if (!col.gameObject.CompareTag(VirtualMapConstants.VIRTUAL_PLAYER_TAG))
			{
				return;
			}

			_onTriggerEnter?.Invoke(this);
			// VirtualMapEvent.InvokeTriggerEnter(this);
		}

		private void OnTriggerExit2D(Collider2D col)
		{
			if (!col.gameObject.CompareTag(VirtualMapConstants.VIRTUAL_PLAYER_TAG))
			{
				return;
			}

			_onTriggerExit?.Invoke(this);
			// VirtualMapEvent.InvokeTriggerExit(this);
		}
	}
}