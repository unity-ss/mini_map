﻿using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Slar.Virtual
{
	public class AVirtualMapMarker : MonoBehaviour
	{
		[SerializeField] private CircleCollider2D _collider;


		[SerializeField] protected SpriteRenderer _spriteRenderer;


		public float ColliderRadius => _collider.radius;
		public Sprite Sprite => _spriteRenderer.sprite;

		public Color Color
		{
			get => _spriteRenderer.color;
			private set => _spriteRenderer.color = value;
		}

		public Vector2 WorldPosition => new Vector2(transform.position.x, transform.position.y);
		public Vector2 RelativePosition { get; set; }


		public void Init()
		{
			Color = GetRandomColor();
		}

		public virtual void SetScale(float scale)
		{
			transform.localScale = scale * Vector3.one;
		}


		private Color GetRandomColor()
		{
			var r = Random.Range(0, 1f);
			var g = Random.Range(0, 1f);
			var b = Random.Range(0, 1f);

			return new Color(r, g, b);
		}
	}
}