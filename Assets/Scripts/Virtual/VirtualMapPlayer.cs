﻿using UnityEngine;

namespace Slar.Virtual
{
	public class VirtualMapPlayer : AVirtualMapMarker
	{
		[SerializeField] private float _speed;
		[SerializeField] private Rigidbody2D _rb2d;
		[SerializeField] private CircleCollider2D _spriteCollider;


		public float SpriteRadius => _spriteCollider.radius;


		public override void SetScale(float scale)
		{
			_spriteRenderer.transform.localScale = Vector3.one * scale;
		}


		private void Update()
		{
			var speed = _speed * Time.deltaTime;

			var h = Input.GetAxis("Horizontal") * speed;
			var v = Input.GetAxis("Vertical")   * speed;

			var velocity = new Vector2(h, v);

			transform.Translate(velocity);
			// _rb2d.velocity += velocity;
		}
	}
}